#include "DualMC33926MotorShield.h" //include the library for the motor driver
#include <MsTimer2.h>//hardcore timer
#include "Wire.h"                 
#include "I2Cdev.h"
#include "MPU6050.h"

#define LED_PIN 13
#define enc_left 2    //left wheel interrupt pin
#define enc_right 3   //right

// DEFINE GLOBALS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DualMC33926MotorShield md;
MPU6050 mpu;  

int16_t ax, ay, az;  // define accel as ax,ay,az
int16_t gx, gy, gz;  // define gyro as gx,gy,gz

//////////////////////////////////////////

volatile float pwmm=0;
volatile float pwmmR=0;
volatile float pwmmL=0;

volatile float oangle=0;
volatile float turnDiff=0;

//////////////////////////////////////////

volatile float velR=0;
volatile float velL=0;

volatile float velRreadings[10];      // the readings from the analog input
volatile int readIndex = 0;           // the index of the current reading
volatile float velRtotal = 0;         // the running total
volatile float velRaverage = 0;       // the average

volatile float velLreadings[10];      // the readings from the analog input
volatile float velLtotal = 0;         // the running total
volatile float velLaverage = 0;       // the average

//////////////////////////////////////////The variables for the current position and the orientation 

float currentTheta = 0;
float currentX = 0;
float currentY = 0;
float currentDis = 0;

//////////////////////////////////////////
volatile float thetaControl = 0;
volatile float disControl = 0;


///////////////////////////////////////

//int time100ms=0;

//int potential = analogRead(A2);

/////////////////////////////////////////

volatile long enc_count_left = 0;
volatile long enc_count_right = 0;

volatile long enc_count_left_pre = 0;
volatile long enc_count_right_pre = 0;

volatile long enc_count_left_pre_d = 0;
volatile long enc_count_right_pre_d = 0;

volatile bool trailingOrdLpre=digitalRead(5);
volatile bool trailingOrdRpre=digitalRead(6);

volatile int count10ms=0;


// ENCODER ISR METHODS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//pins 2 and 5
void encoder_isr_left(){
  
  if(trailingOrdLpre!=digitalRead(5)){
    
    trailingOrdLpre=digitalRead(5);
    bool int2=digitalRead(2);
    if(int2==0&&trailingOrdLpre==0)
      enc_count_left++;
    else if (int2==1&&trailingOrdLpre==1)
      enc_count_left++;
    else if (int2==0&&trailingOrdLpre==1)
      enc_count_left--;
    else if (int2==1&&trailingOrdLpre==0)
      enc_count_left--;
  }
  
  trailingOrdLpre=digitalRead(5);

  
//  Serial.print(digitalRead(2));
//  Serial.println(digitalRead(5));
}


//pins 3 and 6
void encoder_isr_right(){
  
  if(trailingOrdRpre!=digitalRead(6)){
    
    trailingOrdRpre=digitalRead(6);
    if(digitalRead(3)==0&&trailingOrdRpre==0)
        enc_count_right++;
    else if (digitalRead(3)==1&&trailingOrdRpre==1)
       enc_count_right++;
    else if (digitalRead(3)==0&&trailingOrdRpre==1)
        enc_count_right--;
    else if (digitalRead(3)==1&&trailingOrdRpre==0)
        enc_count_right--;
  }
  
  trailingOrdRpre=digitalRead(6);
//
//   Serial.print(digitalRead(3));
//  Serial.println(digitalRead(6));

}


// TIMER BALANCE ISR METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//balance
void balance() {
  count10ms++;
  int potential = analogRead(A2);

  //reference angle = arctan ( (acceleration of x * 2g/2^15) / (acceleration of z * 2g/2^15 + ) ) * 
  float angle = atan((float(ax)*2/32768.0)/(float(az)*2/32768.0+0.16))*57.29;

  //oangle = ( 0.5*(enc_count_left+enc_count_right-0) + 40*(velRaverage+velLaverage) );

  //new turn differential = old turnDiff + 0.01 * (enc_count difference) + 5 * (velocity_average distance)
  turnDiff+=0.03*(enc_count_left-enc_count_right+thetaControl*22.74)+3*(velLaverage-velRaverage);//35.7 for 90 degree
   //turnDiff+=0.03*(currentTheta-1.57)+3*(velLaverage-velRaverage);
  
  //turnDiff+=0.1*((currentTheta + acos(currentX/sqrt(fabs(currentX*currentX + currentY*currentY))))/2)+3*(velLaverage-velRaverage);

  //if(fabs(velRaverage+velLaverage)>0.8)
    //oangle=0;

//  if (turnDiff > 100 ){
//    turnDiff=100;
//  }
//  if (turnDiff < -100){
//    turnDiff=-100;
//  }


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//  oangle = 0.06*(enc_count_left+enc_count_right-100) + 10*(velRaverage+velLaverage);

  float oangleMax = 2;

//  if(abs(enc_count_left+enc_count_right-disControl)<20){
//      oangle = ( 0.1*(enc_count_left+enc_count_right-disControl) + 10*(velRaverage+velLaverage) );//0.09 and 10
//      oangleMax = 8;
//      
//  }
//  else{
//      oangle = ( 0.5*(enc_count_left+enc_count_right-disControl)+80*(velRaverage+velLaverage));
//      oangleMax = 5;
//      //digitalWrite(LED_PIN,LOW);
//  }
//currentX
    if(abs(currentDis-disControl)<5){
      oangle = ( 0.5*(currentDis-disControl) + 20*(velRaverage+velLaverage) );//0.09 and 10
      oangleMax = 6;
      
  }
  else{
      oangle = ( 0.3*(currentDis-disControl)+80*(velRaverage+velLaverage));
      oangleMax = 4;
      //digitalWrite(LED_PIN,LOW);
  }

  if (oangle > oangleMax ){
    oangle=oangleMax;
  }
  if (oangle < -oangleMax){
    oangle=-oangleMax;
  }
  
//Start from here I will write down something to control the speed.

//oangle=5*(velRaverage+velLaverage-.1);
//
//  if (oangle > oangleMax ){
//    oangle=oangleMax;
//  }
//  if (oangle < -oangleMax){
//    oangle=-oangleMax;
//  }


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//if it lean Green direction turn up the refer angle, add some number
//if it leans Red direction turn down the refer angle, subtract some number -1.62
//So if you want to drive to Red direction add some number
//if you want to drive to Green direction substract some number

//  pwmm += (-angle-1.62-5*(potential/1023.0))*1.2 + (float(gy) * 2000/32768.0)*0.04;//code for finding the ref angle
//  Serial.println(potential);//code for finding the ref angle

  
  pwmm += (-angle-3.33+oangle)*1.2 + (float(gy) * 2000/32768.0)*0.04;//if you wanna turn set the ref angle to 4.11pre=0.013
   
  if (pwmm > 300)
  {
    pwmm=300;
  }
  if (pwmm<-300)
  {
    pwmm=-300;
  }
  

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


  velRtotal = velRtotal - velRreadings[readIndex];
  velLtotal = velLtotal - velLreadings[readIndex];
  
  // read from the sensor:
  velRreadings[readIndex] = float(enc_count_right-enc_count_right_pre);
  enc_count_right_pre=enc_count_right;
  velLreadings[readIndex] = float(enc_count_left-enc_count_left_pre);
  enc_count_left_pre=enc_count_left;
  
  // add the reading to the total:
  velRtotal = velRtotal + velRreadings[readIndex];
  velLtotal = velLtotal + velLreadings[readIndex];
  
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array... wrap around to the beginning:
  if (readIndex >= 10) {
    readIndex = 0;
  }

  // calculate the average:
  velRaverage = velRtotal / 10;
  velLaverage = velLtotal / 10;

////////////////////////////////////////////////////////////////////////////
  
  pwmmR=pwmm+turnDiff;
  if (pwmmR>400)
    pwmmR=400;
  if (pwmmR<-400)
    pwmmR=-400;
    
  pwmmL=pwmm-turnDiff;
  if (pwmmL>400)
    pwmmL=400;
  if (pwmmL<-400)
    pwmmL=-400;
//////////////////////////////////////////////////////////////////////////code for fall over power down

if(fabs(angle)>50)
{
    pwmmL=0;
    pwmmR=0;  
}



////////////////////////////////////////////////////////////////////////////

  md.setM1Speed(int(pwmmR));
  md.setM2Speed(int(pwmmL)); 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  
//  Serial.println(potential);
//    Serial.print("pwmm:\t");
//     Serial.print(pwmm);
//     Serial.print("\t");
//     Serial.print("pwmmR:\t");
//     Serial.print(pwmmR);
//     Serial.print("\t");
//     Serial.print("pwmmt:\t");
//     Serial.println(pwmmt);
//  Serial.print("Inte_left:\t");
//  Serial.print(enc_count_left);
//  Serial.print("\t");
//  Serial.print("Inte_right:\t");
//  Serial.println(enc_count_right);
//
//  Serial.print("velavg:\t");
//  Serial.println(velRaverage);
  //Serial.println(potential);

}


// SETUP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  
  Wire.begin();      // join I2C bus   
  Serial.begin(9600);    //  initialize serial communication
  Serial.println("Initializing I2C devices...");
  mpu.initialize();  
// mpu.setZAccelOffset(2621); // 1688 factory default for my test chip


  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  pinMode(LED_PIN, OUTPUT);  // configure LED pin

  md.init();

  pinMode(2, INPUT);  
  pinMode(3, INPUT);  
  
  pinMode(5, INPUT);  
  pinMode(6, INPUT);  
  
  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  digitalWrite(5, HIGH); 
  digitalWrite(6, HIGH); 
  
  attachInterrupt(digitalPinToInterrupt(enc_left),encoder_isr_left,CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc_right),encoder_isr_right,CHANGE);
  

  for (int thisReading = 0; thisReading < 10; thisReading++)
  {
    velRreadings[thisReading] = 0;
    velLreadings[thisReading] = 0;
  }
  
  MsTimer2::set(10, balance); // 10ms period
  MsTimer2::start();
  
}
//goStraight
float subGoalX[]={34,59,68};
float subGoalY[]={-9,-34,-68};
int segCount=0;
float segTheta[]={0,0.523599,1.047198,1.570797,1.570797,1.8325965,2.094396,2.3561955,2.617995,2.8797945,3.141594,3.4033935,3.665193,3.9269925,4.188792,4.4505915,4.712391,4.9741905,5.23599,5.4977895,5.759589,6.0213885,6.283188,6.283188,5.759589,5.23599,4.712391,4.188792,3.665193,3.141594,3.141594
};
float segDisC[]={150,150,150,150,150,150,150,150,200,200,200,200,200,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150};
float segDis[]={-1,240,268,296,324,392,420,448,476,504,532,560,588,616,644,672,700,728,756,784,812,840,868,896,1136,1164,1192,1220,1248,1276,1304,1724};
int danceFlag=1;
//float segTheta[]={0, 0.523,1.05,1.57};
//float segDisC[]={150,100,100,100};
//float segDis[]={-1,100,128,156,184};

// LOOP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float twopointDis(float aX, float aY, float bX, float bY){
  return sqrt(pow((aX-bX),2)+pow((aY-bY),2));
  }
//
//
float tempTheta=1;

void loop() {
  
  // read measurements from device
  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

  // dsL, dsR, dsAvg, dTheta (should we maybe just hardcode these into the below calculations? for speed?)
  float dsL=enc_count_left-enc_count_left_pre_d;
  float dsR=enc_count_right-enc_count_right_pre_d;
  float dsAvg = (dsL+dsR)/2.0;
  float dTheta = float((dsL-dsAvg))/11.39; // the 11.39 is the r of the car in oreo


  // update current theta, wraparound at 2pi
  currentTheta += dTheta;
  
  if (currentTheta > 3.14){
    currentTheta-=6.283;
  }
  else if (currentTheta < -3.14){
    currentTheta+=6.283;
  }

  // update currX, currY
  currentX     += dsAvg*cos(currentTheta);
  currentY     += dsAvg*sin(currentTheta);
  currentDis   += dsAvg;

  //disControl=400;
  // keep track of last counts
  enc_count_left_pre_d = enc_count_left;
  enc_count_right_pre_d = enc_count_right;

//  if(abs(enc_count_left+enc_count_right-disControl)<20)
//  {
//    digitalWrite(LED_PIN,HIGH);
//    }
//  else{
//    
//    digitalWrite(LED_PIN,LOW);
//    }

//thetaControl=1.57;

//float tempDis=sqrt(pow(currentX-));
//  if(currentDis>100)
//  {
//
////     if(millis()%50==0)
////     {
////         tempTheta+=0.05;
////     }
//      //disControl=100;
//    digitalWrite(LED_PIN,HIGH);
//     
//     
//     //Serial.println("bnmvbnmvbn");
//  }
//   else
//   {
//
//     if(millis()%50==0)
//     {
//         tempTheta+=0.7;
//         if(tempTheta>1.57)
//            tempTheta=1.57;
//
//         //disControl=currentDis;
//     }
//     //d
//     //tempTheta=disControl/68.0;
//     float subGoalX=68*sin(tempTheta);
//     float subGoalY=68*cos(tempTheta)-68;
//     
//     disControl=twopointDis(currentX,currentY,subGoalX,subGoalY);
//
//     
//     if(subGoalX-currentX!=0)
//         thetaControl=-currentTheta-atan((subGoalY-currentY)/(subGoalX-currentX)); 
//
//         digitalWrite(LED_PIN,LOW);
//   }

//if(currentDis<240)
//{
//  disControl=currentDis+150;
//  }
//else if(currentDis>240&&currentDis<268)
//{
//      disControl=350;
//      thetaControl=0.523;
//        
//}
//
//else if(currentDis>268&&currentDis<296)
//{
//      disControl=400;
//      thetaControl=1.05;
//  }
//else if(currentDis>296&&currentDis<324)
//{
//      disControl=500;
//      thetaControl=1.57;
//  }
//  else if(currentDis>324&&currentDis<384)
//  {
//    disControl=currentDis+150;
//    }
//    else
//    {
//      disControl=384;
//      }

if(currentDis<segDis[segCount+1])
{
  disControl=currentDis+segDisC[segCount];
  thetaControl=segTheta[segCount];
   if(segCount==1||segCount==4||segCount==5||segCount==23||segCount==24||segCount==30)
      {
        
//        if(count10ms<1000)
//            {
//             //if(count10ms==100)
////             if(currentDis<segDis[segCount])
////                disControl=currentDis+30;
////              else
////                disControl=currentDis+30;
//              //disControl=currentDis;
//              disControl=segDis[segCount];
//              thetaControl=segTheta[segCount-1];
//            }
        }
  
  }
else{
  
  segCount++;
    if(segCount>30)
    {
      segCount=30;
      disControl=currentDis;
      danceFlag++;
      //thetaControl=1;
         thetaControl+=0.005;        
//      if(danceFlag>32700)
//      {
//         thetaControl=3;
//         danceFlag=-32700;
//      }
//      if(danceFlag==0)
//      {
//        thetaControl=1;
//        }

      

        
      }
      
 
      
     

        count10ms=0;
  }
  
    
       //digitalWrite(LED_PIN,HIGH);
    




//if(twopointDis(currentX, currentY,subGoalX[segCount], subGoalY[segCount] )<10)
//{
//      disControl=currentDis+100;
//      //thetaControl=0.523;
//      segCount+=1;
//      if (segCount>2)
//           {segCount=2;
//           disControl=84;
//           
//           }
//      thetaControl=-currentTheta-atan((subGoalY[segCount]-currentY)/(subGoalX[segCount]-currentX)); 
//        
//}
//
//else if(currentDis>28&&currentDis<56)
//{
//      disControl=150;
//      thetaControl=1.05;
//  }
//else if(currentDis>56&&currentDis<84)
//{
//      disControl=200;
//      thetaControl=1.57;
//  }
//  else
//  {
//    disControl=84;
//    }


//if(currentDis>segDis[segCount]&&currentDis<segDis[segCount+1])
//{
//      disControl=currentDis+80;
//      thetaControl=segTheta[segCount];
//  }
//  else{
//    segCount++;
//    if (segCount>1)
//       { segCount=1;
//         thetaControl=segTheta[2];
//         disControl=segDis[3];
//       }
//    }

//   if(currentY<-100)
//   {
//    digitalWrite(LED_PIN,LOW);
//    }
//    else
//    digitalWrite(LED_PIN,HIGH);


//disControl=200;
//if(abs(enc_count_left+enc_count_right-disControl)<20)
//   {disControl=400;
//   thetaControl=35.7;
//   
//   }

// if(abs(enc_count_left+enc_count_right-disControl)<20)
//   {disControl=600;
//   thetaControl=71.4;
//   
//   }
//
//  Serial.print("currentTheta: \t"); Serial.print(currentTheta);
//  Serial.print(", currentX: "); Serial.print(currentX);
//  Serial.print(", currentY: "); Serial.println(currentY);
//  Serial.print(", currentDisddd: "); Serial.print((enc_count_left+enc_count_right)/2.0);
//  Serial.print(", currentDis: "); Serial.println(currentDis);

//  delay(100);
  
//  Serial.print("Inte_left:\t"); Serial.print(enc_count_left);
//  Serial.print("\t"); Serial.print("Inte_right:\t");
//  Serial.print(enc_count_right); Serial.print("\n");
}
