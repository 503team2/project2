# README #

Project Description:

  Add something here like: This is UMass Amherst CS 503 Team2's balancer robot it consists of ...

Arduino Libraries and how to add them:

 - motor driver library:
   
     "DualMC33926MotorShield.h"

     1. In the Arduino IDE, open the "Sketch" menu, select "Include Library", then "Manage Libraries...".
     2. Search for "DualMC33926MotorShield".
     3. Click the DualMC33926MotorShield entry in the list.
     4. Click "Install".

 - IR library
     
     http://www.pololu.com/file/download/PololuQTRSensorsForArduino.zip?file_id=0J403

     1. Extract this zip file to your arduino-00xx/libraries directory.
     2. I had to modify the #include "WConstants.h" in PololuQTRSensors.cpp to #include "Arduino.h" to get them working. I'm on a Mac, also.
     3. You'll probably need to restart the IDE for the library to show up.


 - IMU library:

    http://diyhacking.com/projects/MPU6050.zip

    http://diyhacking.com/projects/I2Cdev.zip

    To test the Arduino MPU 6050, first download the Arduino library for MPU 6050, developed by Jeff Rowberg. You can find the library here. Next you have to unzip/extract this library and take the folder named “MPU6050” and paste it inside the Arduino’s “library” folder. To do this, go to the location where you have installed Arduino (Arduino –> libraries) and paste it inside the libraries folder. You might also have to do the same thing to install the I2Cdev library if you don’t already have it for your Arduino. Do the same procedure as above to install it, you can find the file here: I2Cdev library.

    If you have done this correctly, when you open the arduino IDE, you can see “MPU6050” in File –> Examples. Next, open the example program from: File –> Examples –> MPU6050 –> Examples –> MPU6050_DMP6.