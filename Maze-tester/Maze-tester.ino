#include "DualMC33926MotorShield.h" //include the library for the motor driver
#include <MsTimer2.h>//hardcore timer
#include "Wire.h"                 
#include "I2Cdev.h"
#include "MPU6050.h"

//#define LED_PIN 13


#define enc_left 2    //left wheel interrupt pin
#define enc_right 3   //right

#define pingPinLeft 11
#define pingPinFront 13

// DEFINE GLOBALS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DualMC33926MotorShield md;
MPU6050 mpu;  

int16_t ax, ay, az;  // define accel as ax,ay,az
int16_t gx, gy, gz;  // define gyro as gx,gy,gz

//////////////////////////////////////////

volatile float pwmm=0;
volatile float pwmmR=0;
volatile float pwmmL=0;

volatile float oangle=0;
volatile float turnDiff=0;

//////////////////////////////////////////
volatile float velR=0;
volatile float velL=0;

volatile float velRreadings[10];      // the readings from the analog input
volatile int readIndex = 0;           // the index of the current reading
volatile float velRtotal = 0;         // the running total
volatile float velRaverage = 0;       // the average

volatile float velLreadings[10];      // the readings from the analog input
volatile float velLtotal = 0;         // the running total
volatile float velLaverage = 0;       // the average

//////////////////////////////////////////The variables for the current position and the orientation 
volatile float accelR = 0;
volatile float accelL = 0;

volatile float accelRreadings[10];                
volatile float accelRtotal = 0;         
volatile float accelRaverage = 0;       

volatile float accelLreadings[10];      
volatile float accelLtotal = 0;         
volatile float accelLaverage = 0;
//////////////////////////////////////////
long pingLeft;
long pingFront;
long pingLeft_prev;
long pingFront_prev;
long durationLeft;
long durationFront;

float pingAngle;

//////////////////////////////////////////

float currentTheta = 0;
float currentX = 0;
float currentY = 0;
float currentDis = 0;

int state = 2;

//////////////////////////////////////////
volatile float thetaControl = 0;
volatile float disControl = 0;



///////////////////////////////////////

//int time100ms=0;

int potential = analogRead(A2);

/////////////////////////////////////////

volatile long enc_count_left = 0;
volatile long enc_count_right = 0;

volatile long enc_count_left_start = 0;
volatile long enc_count_right_start = 0;

volatile long enc_count_left_pre = 0;
volatile long enc_count_right_pre = 0;

volatile long enc_count_left_pre_d = 0;
volatile long enc_count_right_pre_d = 0;

volatile bool trailingOrdLpre=digitalRead(5);
volatile bool trailingOrdRpre=digitalRead(6);

volatile int count10ms=0;


// ENCODER ISR METHODS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//pins 2 and 5
void encoder_isr_left(){
  
  if(trailingOrdLpre!=digitalRead(5)){
    
    trailingOrdLpre=digitalRead(5);
    bool int2=digitalRead(2);
    if(int2==0&&trailingOrdLpre==0)
      enc_count_left++;
    else if (int2==1&&trailingOrdLpre==1)
      enc_count_left++;
    else if (int2==0&&trailingOrdLpre==1)
      enc_count_left--;
    else if (int2==1&&trailingOrdLpre==0)
      enc_count_left--;
  }
  
  trailingOrdLpre=digitalRead(5);

  
//  Serial.print(digitalRead(2));
//  Serial.println(digitalRead(5));
}


//pins 3 and 6
void encoder_isr_right(){
  
  if(trailingOrdRpre!=digitalRead(6)){
    
    trailingOrdRpre=digitalRead(6);
    if(digitalRead(3)==0&&trailingOrdRpre==0)
        enc_count_right++;
    else if (digitalRead(3)==1&&trailingOrdRpre==1)
       enc_count_right++;
    else if (digitalRead(3)==0&&trailingOrdRpre==1)
        enc_count_right--;
    else if (digitalRead(3)==1&&trailingOrdRpre==0)
        enc_count_right--;
  }
  
  trailingOrdRpre=digitalRead(6);
//
//   Serial.print(digitalRead(3));
//  Serial.println(digitalRead(6));

}


// TIMER BALANCE ISR METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//balance
void balance() {
  count10ms++;
  //int potential = analogRead(A2);

  //reference angle = arctan ( (acceleration of x * 2g/2^15) / (acceleration of z * 2g/2^15 + ) ) * 
  float angle = atan((float(ax)*2/32768.0)/(float(az)*2/32768.0+0.16))*57.29;

  //oangle = ( 0.5*(enc_count_left+enc_count_right-0) + 40*(velRaverage+velLaverage) );

  //new turn differential = old turnDiff + 0.01 * (enc_count difference) + 5 * (velocity_average distance)
   //turnDiff+=0.03*(enc_count_left-enc_count_right+thetaControl*22.74)+3*(velLaverage-velRaverage);//35.7 for 90 degree

  switch(state){
    case 0: turnDiff+=( 5*((velLaverage - velRaverage) - 0.3) + 100*(accelLaverage - accelRaverage) );break; // total blind constant turn
    
    case 1: /*do something*/;break; //only front sensor, turn
    
    case 2: turnDiff+=0.03*(pingAngle + ((enc_count_left - enc_count_left_start)-(enc_count_right - enc_count_right_start)))+3*(velLaverage-velRaverage);
    //turnDiff+=( -0.15*(pingLeft - 15) + 10*(velLaverage-velRaverage));break; //only left sensor, strait
    
    case 3: /*do something*/;break; //both front and left sensor, turn
    
    default: turnDiff+=0.03*((enc_count_left - enc_count_left_start)-(enc_count_right - enc_count_right_start))+3*(velLaverage-velRaverage); //strait
  }

  if (turnDiff > 100 ){
    turnDiff=100;
  }
  if (turnDiff < -100){
    turnDiff=-100;
  }


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//  oangle = 0.06*(enc_count_left+enc_count_right-100) + 10*(velRaverage+velLaverage);

//  float oangleMax = 2;
//  
//  if(abs(currentDis-disControl)<5){
//      oangle = ( 0.5*(currentDis-disControl) + 20*(velRaverage+velLaverage) );//0.09 and 10
//      oangleMax = 6;
//      
//  }
//  else{
//      oangle = ( 0.3*(currentDis-disControl)+80*(velRaverage+velLaverage));
//      oangleMax = 4;
//      //digitalWrite(LED_PIN,LOW);
//  }
//
//  if (oangle > oangleMax ){
//    oangle=oangleMax;
//  }
//  if (oangle < -oangleMax){
//    oangle=-oangleMax;
//  }
//
  float oangleMax = 20;
  
  oangle = ( 10*(velLaverage + velRaverage + 0.5) + 200*(accelLaverage + accelRaverage) );//max speed 2.0, good speed 0.5

  if (oangle > oangleMax ){
    oangle=oangleMax;
  }
  if (oangle < -oangleMax){
    oangle=-oangleMax;
  }
//  
//Start from here I will write down something to control the speed.

//oangle=5*(velRaverage+velLaverage-.1);
//
//  if (oangle > oangleMax ){
//    oangle=oangleMax;
//  }
//  if (oangle < -oangleMax){
//    oangle=-oangleMax;
//  }


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//if it lean Green direction turn up the refer angle, add some number
//if it leans Red direction turn down the refer angle, subtract some number -1.62
//So if you want to drive to Red direction add some number
//if you want to drive to Green direction substract some number

//  pwmm += (-angle-1.62-5*(potential/1023.0))*1.2 + (float(gy) * 2000/32768.0)*0.04;//code for finding the ref angle
//  Serial.println(potential);//code for finding the ref angle
//oangle=0;
  
  pwmm += (-angle-5.5+oangle)*1.2 + (float(gy) * 2000/32768.0)*0.04;//if you wanna turn set the ref angle to 4.11pre=0.013
   
  if (pwmm > 300)
  {
    pwmm=300;
  }
  if (pwmm<-300)
  {
    pwmm=-300;
  }
  

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
  float velRaverage_prev = velRaverage;
  float velLaverage_prev = velLaverage;


  velRtotal = velRtotal - velRreadings[readIndex];
  velLtotal = velLtotal - velLreadings[readIndex];
  
  // read from the sensor:
  velRreadings[readIndex] = float(enc_count_right-enc_count_right_pre);
  enc_count_right_pre=enc_count_right;
  velLreadings[readIndex] = float(enc_count_left-enc_count_left_pre);
  enc_count_left_pre=enc_count_left;
  
  // add the reading to the total:
  velRtotal = velRtotal + velRreadings[readIndex];
  velLtotal = velLtotal + velLreadings[readIndex];

  // calculate the average:
  velRaverage = velRtotal / 10.0;
  velLaverage = velLtotal / 10.0;

  accelRtotal = accelRtotal - accelRreadings[readIndex];
  accelLtotal = accelLtotal - accelLreadings[readIndex];

  accelR = velRaverage - velRaverage_prev;
  accelL = velLaverage - velLaverage_prev;

  accelRreadings[readIndex] = accelR;
  accelLreadings[readIndex] = accelL;

  accelRtotal = accelRtotal + accelRreadings[readIndex];
  accelLtotal = accelLtotal + accelLreadings[readIndex];

  accelRaverage = accelRtotal / 10.0;
  accelLaverage = accelLtotal / 10.0;
  
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array... wrap around to the beginning:
  if (readIndex >= 10) {
    readIndex = 0;
  }

////////////////////////////////////////////////////////////////////////////
  
  pwmmR=pwmm+turnDiff;
  if (pwmmR>400)
    pwmmR=400;
  if (pwmmR<-400)
    pwmmR=-400;
    
  pwmmL=pwmm-turnDiff;
  if (pwmmL>400)
    pwmmL=400;
  if (pwmmL<-400)
    pwmmL=-400;
//////////////////////////////////////////////////////////////////////////code for fall over power down

if(fabs(angle)>50)
{
    pwmmL=0;
    pwmmR=0;  
}



////////////////////////////////////////////////////////////////////////////

  md.setM1Speed(int(pwmmR));
  md.setM2Speed(int(pwmmL)); 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////




//  if(count10ms==15)
//  {
//    // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
//    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
//    pinMode(pingPinLeft, OUTPUT);
//    digitalWrite(pingPinLeft, LOW);
//    delayMicroseconds(200);
//    digitalWrite(pingPinLeft, HIGH);
//    delayMicroseconds(5);
//    digitalWrite(pingPinLeft, LOW);
//    delayMicroseconds(750);
//    // The same pin is used to read the signal from the PING))): a HIGH
//    // pulse whose duration is the time (in microseconds) from the sending
//    // of the ping to the reception of its echo off of an object.
//    pinMode(pingPinLeft, INPUT);
//    durationLeft = pulseIn(pingPinLeft, HIGH,8000);
//    
//    float newPingVal = microsecondsToCentimeters(durationLeft);
//    if (newPingVal != 0){
//      pingLeft_prev = pingLeft;
//      pingLeft = microsecondsToCentimeters(durationLeft);
//    }
//  }
//     if(count10ms>30){
//    pinMode(pingPinFront, OUTPUT);
//    digitalWrite(pingPinFront, LOW);
//    delayMicroseconds(200);
//    digitalWrite(pingPinFront, HIGH);
//    delayMicroseconds(5);
//    digitalWrite(pingPinFront, LOW);
//    delayMicroseconds(750);
//    // The same pin is used to read the signal from the PING))): a HIGH
//    // pulse whose duration is the time (in microseconds) from the sending
//    // of the ping to the reception of its echo off of an object.
//    pinMode(pingPinFront, INPUT);
//    durationFront = pulseIn(pingPinFront, HIGH,8000);
//    
//    float newPingVal = microsecondsToCentimeters(durationFront);
//    if (newPingVal != 0){
//      pingFront_prev = pingFront;
//      pingFront = microsecondsToCentimeters(durationFront);
//    }
//    
//    count10ms=0;
//  }

    
    

}


// SETUP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  
  Wire.begin();      // join I2C bus   
  Serial.begin(9600);    //  initialize serial communication
  Serial.println("Initializing I2C devices...");
  mpu.initialize();  
// mpu.setZAccelOffset(2621); // 1688 factory default for my test chip


  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  

  md.init();

  pinMode(2, INPUT);  
  pinMode(3, INPUT);  
  
  pinMode(5, INPUT);  
  pinMode(6, INPUT);  
  
  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  digitalWrite(5, HIGH); 
  digitalWrite(6, HIGH); 
  
  attachInterrupt(digitalPinToInterrupt(enc_left),encoder_isr_left,CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc_right),encoder_isr_right,CHANGE);
  

  for (int thisReading = 0; thisReading < 10; thisReading++)
  {
    velRreadings[thisReading] = 0;
    velLreadings[thisReading] = 0;
    accelRreadings[thisReading] = 0;
    accelLreadings[thisReading] = 0;
  }
  
  MsTimer2::set(10, balance); // 10ms period
  MsTimer2::start();
  
}

// LOOP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float twopointDis(float aX, float aY, float bX, float bY){
  return sqrt(pow((aX-bX),2)+pow((aY-bY),2));
  }
//
//
long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}


float tempTheta=1;
char flagTurnLeft=0;
char flagTurnRight=0;

void loop() {
  
  // read measurements from device
  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

  if (pingLeft < 50 && pingLeft > 0){
    if (pingFront < 50 && pingFront > 0){
      state = 0;
    }
    else {
      if (state != 2){
        enc_count_left_start = 0;
        enc_count_right_start = 0;
        pingLeft_prev = pingLeft;
      }
      state = 2;
    }
  }
  else {
    if (pingFront < 50 && pingFront > 0){
      state = 0;
    }
    else {
      state = 0;//real 0
    }
  }
  
  if(count10ms==15)
  {
    noInterrupts();
    // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    pinMode(pingPinLeft, OUTPUT);
    digitalWrite(pingPinLeft, LOW);
    delayMicroseconds(200);
    digitalWrite(pingPinLeft, HIGH);
    delayMicroseconds(5);
    digitalWrite(pingPinLeft, LOW);
    delayMicroseconds(750);
    // The same pin is used to read the signal from the PING))): a HIGH
    // pulse whose duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    pinMode(pingPinLeft, INPUT);
    durationLeft = pulseIn(pingPinLeft, HIGH,8000);
    
    float newPingVal = microsecondsToCentimeters(durationLeft);
    //if (newPingVal != 0){
      pingLeft_prev = pingLeft;
      pingLeft = microsecondsToCentimeters(durationLeft);
    //}
    interrupts();
    
    if (pingLeft_prev != 0){
      pingAngle += 0.025*(-pingLeft + 10) + 0.8*(pingLeft_prev - pingLeft);
    }
    
  }
  
  if(count10ms>30){
    noInterrupts();
    pinMode(pingPinFront, OUTPUT);
    digitalWrite(pingPinFront, LOW);
    delayMicroseconds(200);
    digitalWrite(pingPinFront, HIGH);
    delayMicroseconds(5);
    digitalWrite(pingPinFront, LOW);
    delayMicroseconds(750);
    // The same pin is used to read the signal from the PING))): a HIGH
    // pulse whose duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    pinMode(pingPinFront, INPUT);
    durationFront = pulseIn(pingPinFront, HIGH,8000);
    
    float newPingVal = microsecondsToCentimeters(durationFront);
    //if (newPingVal != 0){
      pingFront_prev = pingFront;
      pingFront = microsecondsToCentimeters(durationFront);
    //}
    
    count10ms=0;
    interrupts();
    
//    Serial.print("pingFront: ");
//    Serial.print(pingFront);
//    Serial.print(", pingFront-change: ");
//    Serial.print(pingFront - pingFront_prev);
//    Serial.print(", pingLeft: ");
//    Serial.print(pingLeft);
//    Serial.print(", pingLeft-change: ");
//    Serial.println(pingLeft - pingLeft_prev);
   
  }
}
