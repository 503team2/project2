#define LED_PIN 13
#define enc_left 2    //left wheel interrupt pin
#define enc_right 3   //right



volatile long enc_count_left = 0;
volatile long enc_count_right = 0;

volatile long enc_count_left_pre = 0;
volatile long enc_count_right_pre = 0;

volatile bool trailingOrdLpre=digitalRead(5);
volatile bool trailingOrdRpre=digitalRead(6);

//pins 2 and 5
void encoder_isr_left(){
//  Serial.print(digitalRead(2));
//  Serial.println(digitalRead(5));
  if(trailingOrdLpre!=digitalRead(5)){
    
    trailingOrdLpre=digitalRead(5);
    bool int2=digitalRead(2);
    if(int2==0&&trailingOrdLpre==0)
      enc_count_left--;
    else if (int2==1&&trailingOrdLpre==1)
      enc_count_left--;
    else if (int2==0&&trailingOrdLpre==1)
      enc_count_left++;
    else if (int2==1&&trailingOrdLpre==0)
      enc_count_left++;
  }
  
  trailingOrdLpre=digitalRead(5);
  //Serial.print("Left inpterrupt:");
//  Serial.print(digitalRead(2));
//  Serial.println(digitalRead(5));

  
}


//pins 3 and 6
void encoder_isr_right(){
  //Serial.print("Right inpterrupt:");
// 
    Serial.print(digitalRead(3));
  Serial.println(digitalRead(6));
  if(trailingOrdRpre!=digitalRead(6)){
    
    trailingOrdRpre=digitalRead(6);
    if(digitalRead(3)==0&&trailingOrdRpre==0)
        enc_count_right--;
    else if (digitalRead(3)==1&&trailingOrdRpre==1)
       enc_count_right--;
    else if (digitalRead(3)==0&&trailingOrdRpre==1)
        enc_count_right++;
    else if (digitalRead(3)==1&&trailingOrdRpre==0)
        enc_count_right++;
  }
  
  trailingOrdRpre=digitalRead(6);




}


void setup() {
  
  
  Serial.begin(115200);  

  pinMode(LED_PIN, OUTPUT);  // configure LED pin



  pinMode(2, INPUT);  
  pinMode(3, INPUT);  
  
  pinMode(5, INPUT);  
  pinMode(6, INPUT);  
  
  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  digitalWrite(5, HIGH); 
  digitalWrite(6, HIGH); 
  
  attachInterrupt(digitalPinToInterrupt(enc_left),encoder_isr_left,CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc_right),encoder_isr_right,CHANGE);
}

void loop(){
//delay(100);

//  Serial.print("tau: ");
//  Serial.println(tau);
//
//  Serial.print("phi: "); Serial.print(phi);
//  Serial.print(", currentTheta: "); Serial.print(currentTheta);
//  Serial.print(", currentX: "); Serial.print(currentX);
//  Serial.print(", currentY: "); Serial.println(currentY);
//
//  delay(50);
  
//  Serial.print("Inte_left:\t"); Serial.print(enc_count_left);
//  Serial.print("\t"); Serial.print("Inte_right:\t");
//  Serial.print(enc_count_right); Serial.print("\n");

  //Serial.print(digitalRead(2)); Serial.print(digitalRead(5)); Serial.print(digitalRead(3)); Serial.println(digitalRead(6));

}
