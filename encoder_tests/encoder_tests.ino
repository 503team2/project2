#include "DualMC33926MotorShield.h" //include the library for the motor driver
#include <PololuQTRSensors.h> //library for IR sensors

DualMC33926MotorShield md;

#include "Wire.h"                 
#include "I2Cdev.h"
#include "MPU6050.h"
MPU6050 mpu;  
int16_t ax, ay, az;  // define accel as ax,ay,az
int16_t gx, gy, gz;  // define gyro as gx,gy,gz

#define LED_PIN 13

bool blinkState = false;

float pwmm=0;
int pwmm1=0;//pwm number for the motor
int pwmm2=0;

int potential = analogRead(A2);

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#define enc_left 2 //left wheel interrupt pin
#define enc_right 3 //right


//create object for 2 sensors (single encoder)
PololuQTRSensorsRC qtrrc_left((unsigned char[]) {enc_left,5}, 2);
PololuQTRSensorsRC qtrrc_right((unsigned char[]) {enc_right,4}, 2);

volatile long enc_count_left = 0;
volatile long enc_count_right = 0;

//method triggered by the left interrupt
//pins 2 and 5
void encoder_isr_left(){
  
  static int8_t lookup_table[] = {0,0,0,-1,0,0,1,0,0,1,0,0,-1,0,0,0};
  static uint8_t enc_val = 0;

  enc_val = enc_val << 2; //0000old 00old00new
  uint8_t l1 = ( (PIND & 0b100100) << 3 ) >> 5;
  uint8_t l2 = (PIND & 0b100100) >> 5;
  
  enc_val = enc_val | (l1*0b10+l2);

  enc_count_left = enc_count_left + lookup_table[enc_val & 0b1111];

  Serial.print("direction left: ");
  Serial.println(lookup_table[enc_val & 0b1111]);
 
}


//pins 3 and 6
void encoder_isr_right(){
  
  static int8_t lookup_table[] = {0,0,0,-1,0,0,1,0,0,1,0,0,-1,0,0,0};
  static uint8_t enc_val = 0;

  enc_val = enc_val << 2;
  uint8_t l1 = ( (PIND & 0b01001000) << 2 ) >> 5;
  uint8_t l2 = (PIND & 0b01001000) >> 6;
  
  enc_val = enc_val | (l1*0b10+l2);

  enc_count_right = enc_count_right + lookup_table[enc_val & 0b1111];

  //Serial.print("direction right: ");
  //Serial.println(lookup_table[enc_val & 0b1111]);
 
}



void setup() {
  
  Wire.begin();      // join I2C bus   
  Serial.begin(115200);    //  initialize serial communication
  Serial.println("Initializing I2C devices...");
  mpu.initialize();  
 // mpu.setZAccelOffset(2621); // 1688 factory default for my test chip


  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  pinMode(LED_PIN, OUTPUT);  // configure LED pin

  md.init();



  //encoder setup:

  pinMode(enc_left, INPUT);//not sure if these are necessary, I added while troubleshooting pin 3
  pinMode(enc_right, INPUT);

  Serial.println("Calibrating IR sensors, turn the wheels.");
  for (int i = 0; i < 200; i++)  // make the calibration take about 5 seconds
  {
    qtrrc_left.calibrate(); //expose to both shades of the wheel
    qtrrc_right.calibrate();
  }
  
  Serial.println("Done calibrating");

  attachInterrupt(digitalPinToInterrupt(enc_left),encoder_isr_left,CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc_right),encoder_isr_right,CHANGE);
  
}

void loop() {

  
  Serial.print("enc_count_left: ");
  Serial.println(enc_count_left);
  
  Serial.print("enc_count_right: ");
  Serial.println(enc_count_right);

  /*
  unsigned int sensor_values_left[2];
  qtrrc_left.read(sensor_values_left);

  Serial.println("\n\nSensor values left: ");
  
  for (int i = 0; i < 2; i++)
  {
    Serial.print(sensor_values_left[i]);
    Serial.print(' ');
  }

  unsigned int sensor_values_right[2];
  qtrrc_right.read(sensor_values_right);

  Serial.println("\nSensor values right: ");

  for (int i = 0; i < 2; i++)
  {
    Serial.print(sensor_values_right[i]);
    Serial.print(' ');
  }
  */

  Serial.print("PIN 3: ");
  Serial.println(digitalRead(3));

  Serial.print("\nPIN 4: ");
  Serial.println(digitalRead(4));

  Serial.print("PIN 2: ");
  Serial.println(digitalRead(2));

  Serial.print("\nPIN 5: ");
  Serial.println(digitalRead(5)); 

  delay(200);

}