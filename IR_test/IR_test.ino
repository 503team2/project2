#include "DualMC33926MotorShield.h" //include the library for the motor driver
#include <MsTimer2.h>//hardcore timer
#include "Wire.h"                 
#include "I2Cdev.h"
#include "MPU6050.h"

#define LED_PIN 13
#define enc_left 2    //left wheel interrupt pin
#define enc_right 3   //right

// DEFINE GLOBALS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DualMC33926MotorShield md;
MPU6050 mpu;  

int16_t ax, ay, az;  // define accel as ax,ay,az
int16_t gx, gy, gz;  // define gyro as gx,gy,gz

//////////////////////////////////////////

volatile float pwmm=0;
volatile float pwmmR=0;
volatile float pwmmL=0;

volatile float oangle=0;
volatile float turnDiff=0;

//////////////////////////////////////////

volatile float velR=0;
volatile float velL=0;

volatile float velRreadings[10];      // the readings from the analog input
volatile int readIndex = 0;           // the index of the current reading
volatile float velRtotal = 0;         // the running total
volatile float velRaverage = 0;       // the average

volatile float velLreadings[10];      // the readings from the analog input
volatile float velLtotal = 0;         // the running total
volatile float velLaverage = 0;       // the average

//////////////////////////////////////////The variables for the current position and the orientation 

float currentTheta = 0;
float currentX = 0;
float currentY = 0;
float currentDis = 0;

//////////////////////////////////////////

//int time100ms=0;

//int potential = analogRead(A2);

/////////////////////////////////////////

volatile long enc_count_left = 0;
volatile long enc_count_right = 0;

volatile long enc_count_left_pre = 0;
volatile long enc_count_right_pre = 0;

volatile long enc_count_left_pre_d = 0;
volatile long enc_count_right_pre_d = 0;

volatile bool trailingOrdLpre=digitalRead(5);
volatile bool trailingOrdRpre=digitalRead(6);

/////////////////////////////////////////

int numSegments = 0;

float goalTheta = 0;
float goalX = 0;
float goalY = 0;

float startTheta = 0;
float startX = 0;
float startY = 0;

float carrotTheta = 0;
float carrotX = 0;
float carrotY = 0;

float currentPathRad = 0;
float currentPathDist = 0;

float carrotDistance;
int currentSegment;

float phi = 0.0;
float tau = 0.0;
float distanceCarrot=0;

float oreoToCm(float oreo){
  return oreo*0.733;
}

float cmToOreo(float cm){
  return cm/0.733;
}

float radToDeg(float rad){
  return rad*(180.0/3.1415926);
}

float degToRad(float deg){
  return deg*(3.1415926/180.0);
}

float degToOreo(float deg){
  return deg*(60/90);//35.7
}

// ENCODER ISR METHODS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//pins 2 and 5
void encoder_isr_left(){
  
  if(trailingOrdLpre!=digitalRead(5)){
    
    trailingOrdLpre=digitalRead(5);
    bool int2=digitalRead(2);
    if(int2==0&&trailingOrdLpre==0)
      enc_count_left--;
    else if (int2==1&&trailingOrdLpre==1)
      enc_count_left--;
    else if (int2==0&&trailingOrdLpre==1)
      enc_count_left++;
    else if (int2==1&&trailingOrdLpre==0)
      enc_count_left++;
  }
  
  trailingOrdLpre=digitalRead(5);
}


//pins 3 and 6
void encoder_isr_right(){
  
  if(trailingOrdRpre!=digitalRead(6)){
    
    trailingOrdRpre=digitalRead(6);
    if(digitalRead(3)==0&&trailingOrdRpre==0)
        enc_count_right--;
    else if (digitalRead(3)==1&&trailingOrdRpre==1)
       enc_count_right--;
    else if (digitalRead(3)==0&&trailingOrdRpre==1)
        enc_count_right++;
    else if (digitalRead(3)==1&&trailingOrdRpre==0)
        enc_count_right++;
  }
  
  trailingOrdRpre=digitalRead(6);

}


float oangleSign=1;

// TIMER BALANCE ISR METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//balance
void balance() {

  int potential = analogRead(A2);

  //reference angle = arctan ( (acceleration of x * 2g/2^15) / (acceleration of z * 2g/2^15 + ) ) * 
  float angle = atan((float(ax)*2/32768.0)/(float(az)*2/32768.0+0.16))*57.29;

  //oangle = ( 0.5*(enc_count_left+enc_count_right-0) + 40*(velRaverage+velLaverage) );

  //new turn differential = old turnDiff + 0.01 * (enc_count difference) + 5 * (velocity_average distance)
  //turnDiff+=0.01*(enc_count_left-enc_count_right)+3*(velLaverage-velRaverage);
  distanceCarrot=sqrt(pow((carrotY - currentY),2) +pow((carrotX - currentX),2));

  phi = 0;
  float denom = (carrotY - currentY);

  phi = atan2((carrotY - currentY), (carrotX - currentX));

  phi = fmod(phi, 3.14159*2);

  tau = (phi - currentTheta);

  oangleSign = -1;

  if (tau < -3.14159/2){
    oangleSign = 1;
  }
  else if (tau > 3.14159/2){
    oangleSign = 1;
  }
  
  tau = fmod(tau, 3.14159);

  if (tau < -3.14159/2){
    tau+=3.14159;
  }
  else if (tau > 3.14159/2){
    tau-=3.14159;
  }

  if(distanceCarrot<10){
      turnDiff+=1.2*(carrotTheta-currentTheta) - 5*(velLaverage-velRaverage);
  }
  else{
      turnDiff+=1*tau - 5*(velLaverage-velRaverage);
  }




//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


  float oangleMax;
  
      
  if(distanceCarrot<30){
      oangle = -( 0.1*(distanceCarrot*oangleSign) + 10*(velRaverage+velLaverage) );
      oangleMax = 20;
  }
  else{
      oangle = -( 0.1*(distanceCarrot*oangleSign));
      oangleMax = 3;
  }

  

  if (oangle > oangleMax ){
    oangle=oangleMax;
  }
  if (oangle < -oangleMax){
    oangle=-oangleMax;
  }

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


  pwmm += (-angle-3.33+oangle)*1.2 + (float(gy) * 2000/32768.0)*0.04;//if you wanna turn set the ref angle to 4.11pre=0.013
   
  if (pwmm > 200)
  {
    pwmm=200;
  }
  if (pwmm<-200)
  {
    pwmm=-200;
  }
  

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


  velRtotal = velRtotal - velRreadings[readIndex];
  velLtotal = velLtotal - velLreadings[readIndex];
  
  // read from the sensor:
  velRreadings[readIndex] = float(enc_count_right-enc_count_right_pre);
  enc_count_right_pre=enc_count_right;
  velLreadings[readIndex] = float(enc_count_left-enc_count_left_pre);
  enc_count_left_pre=enc_count_left;
  
  // add the reading to the total:
  velRtotal = velRtotal + velRreadings[readIndex];
  velLtotal = velLtotal + velLreadings[readIndex];
  
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array... wrap around to the beginning:
  if (readIndex >= 10) {
    readIndex = 0;
  }

  // calculate the average:
  velRaverage = velRtotal / 10;
  velLaverage = velLtotal / 10;

////////////////////////////////////////////////////////////////////////////
  
  pwmmR=pwmm+turnDiff;
  if (pwmmR>400)
    pwmmR=400;
  if (pwmmR<-400)
    pwmmR=-400;
    
  pwmmL=pwmm-turnDiff;
  if (pwmmL>400)
    pwmmL=400;
  if (pwmmL<-400)
    pwmmL=-400;
//////////////////////////////////////////////////////////////////////////code for fall over power down

if(fabs(angle)>50)
{
    pwmmL=0;
    pwmmR=0;  
}



////////////////////////////////////////////////////////////////////////////

  md.setM1Speed(int(pwmmR));
  md.setM2Speed(int(pwmmL)); 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


}

void InitulizeTurn(float deg, float radius){
  startX = goalX;
  startY = goalY;
  startTheta = goalTheta;

  goalX = startX + radius*sin(degToRad(deg));
  goalY = startY - (radius - radius*cos(degToRad(deg)));
  goalTheta = degToRad(deg);

  
//  totalDistance = cmToOreo(radius)*degToRad(deg);
//  turnRadius = cmToOreo(radius);
//  encoderLeftStart = enc_count_left;
//  encoderRightStart = enc_count_right;
//  disControl = totalDistance;
}

void updateTurn(){
//  long encoderLeft = enc_count_left - encoderLeftStart;
//  long encoderRight = enc_count_right - encoderRightStart;
//
//  float currentDistance = (encoderLeft + encoderRight)/2.0;
//
//  float newTheta = currentDistance/turnRadius;
//  float distanceToGo = totalDistance - currentDistance;
//
//  thetaControl = degToOreo(radToDeg(newTheta));
  //disControl = distanceToGo;
}




// SETUP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  
  Wire.begin();      // join I2C bus   
  Serial.begin(9600);    //  initialize serial communication
  Serial.println("Initializing I2C devices...");
  mpu.initialize();  
// mpu.setZAccelOffset(2621); // 1688 factory default for my test chip


  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  pinMode(LED_PIN, OUTPUT);  // configure LED pin

  md.init();

  pinMode(2, INPUT);  
  pinMode(3, INPUT);  
  
  pinMode(5, INPUT);  
  pinMode(6, INPUT);  
  
  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  digitalWrite(5, HIGH); 
  digitalWrite(6, HIGH); 
  
  attachInterrupt(digitalPinToInterrupt(enc_left),encoder_isr_left,CHANGE);
  attachInterrupt(digitalPinToInterrupt(enc_right),encoder_isr_right,CHANGE);
  

  for (int thisReading = 0; thisReading < 10; thisReading++)
  {
    velRreadings[thisReading] = 0;
    velLreadings[thisReading] = 0;
  }
  
  MsTimer2::set(10, balance); // 10ms period
  MsTimer2::start();

//////////////////////////////

  numSegments = 1;
  float segmentInfo[numSegments*2] = {-1, 400};
  currentSegment = 1;
  carrotDistance = 5;
  
  

}


// LOOP METHOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void loop() {

//  if (fabs(currentX - goalX) < 5 && fabs(currentY - goalY) < 5 && fabs(currentTheta - goalTheta) < 5){
//    if (currentSegment == numSegments){
//      delay(1000);
//      //path is finished
//    }
//    else{
//      currentSegment += 1;
//      startTheta = goalTheta; startX = goalX; startY = goalY;
//      //goalTheta = 
//      //goalX = 
//      //goalY = 
//    }
//    
//  }

  //getNewCarrot();

//////////////////////////////////
  
  // read measurements from device
  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

  // dsL, dsR, dsAvg, dTheta (should we maybe just hardcode these into the below calculations? for speed?)
  float dsL=enc_count_left-enc_count_left_pre_d;
  float dsR=enc_count_right-enc_count_right_pre_d;
  float dsAvg = (dsL+dsR)/2.0;
  float dTheta = float((dsL-dsAvg))/11.39; // the 11.39 is the r of the car in oreo


  // update current theta, wraparound at 2pi
  //currentTheta += dTheta;
  currentTheta = (enc_count_left - enc_count_right) * (3.14159/67.6);
  
//  if (currentTheta > 3.14159){
//    currentTheta-=2*3.14159;
//  }
//  else if (currentTheta < -3.14159){
//    currentTheta+=2*3.14159;
//  }

  // update currX, currY
  currentX     += dsAvg*cos(currentTheta);
  currentY     += dsAvg*sin(currentTheta);
  currentDis   += dsAvg;
  

  // keep track of last counts
  enc_count_left_pre_d = enc_count_left;
  enc_count_right_pre_d = enc_count_right;


if(currentDis>400)
{
  
  }
else if (carrotX-currentDis<10)
{
  carrotX = currentDis+75;
}

//  Serial.print("tau: ");
//  Serial.print(tau);
//    Serial.println(distanceCarrot);

//  Serial.print(" oangleSign: ");
//  Serial.println(oangleSign);
//
//  Serial.print("  phi: "); Serial.print(phi);
//  Serial.print(", currentTheta: "); Serial.print(currentTheta);
//  Serial.print(", phi + currentTheta: "); Serial.println(  currentTheta-phi);


  
//  Serial.print(", currentX: "); Serial.print(currentX);
//  Serial.print(", currentY: "); Serial.print(currentY);
//  Serial.print(", currentTheta: "); Serial.println(currentTheta);
//
//  delay(50);
  
//  Serial.print("Inte_left:\t"); Serial.print(enc_count_left);
//  Serial.print("\t"); Serial.print("Inte_right:\t");
//  Serial.print(enc_count_right); Serial.print("\n");

  //Serial.print(digitalRead(2)); Serial.print(digitalRead(5)); Serial.print(digitalRead(3)); Serial.println(digitalRead(6));

}
